It all began in January 2019. After years of research and idea development, the founder of Hemp Futures and CEO Steve Wyatt commissioned over 68 hectares of fertile, organic soil in Estonia, near the Russian border. It was where he cultivated EU certified Finola seeds, of which we engineered our CBD Warming Gel, CBD Cooling Gel, 5% CBD Oil, and 10% CBD Oil products.

Why choose Finola hemp? It was the first oilseed variety of hemp to be officially recognized by both Canada and the EU. Also, Finola produces high-quality CBD oil that is mainly free of the psychoactive chemical THC. In other words, Steve chose a rich source of pure, non-hallucinogenic cannabidiol.

Only the best would do, for the products that Steve began to hand-craft and market. His aim was and still is to offer pure, natural CBD with genuine medicinal qualities, making it available and affordable for as many international customers are possible.

Our ethics include personally overseeing the cultivation of hemp that does not contain psychoactive elements. Instead, we optimize the potential of the finest hemp strain to yield the best CBD oil. Then, we manage our entire ranges production process, building our global brand by providing an authentically high-quality and medically-beneficial form of CBD.

In 2020, Steve and his team located a 7.5-hectare plot of land in Estonia. The top layer of soil was compacted cow manure, and underneath ancient beach floor sand, the soil could not have been better for growing hemp.


Website: https://hempfutures.eu/
